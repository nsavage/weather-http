import json
from flask import Flask, jsonify

from typing import Union

state_file = "/tmp/temp_data.txt"

def create_app(test_config=None) -> "app":
    app = Flask(__name__)

    @app.route("/")
    def hello() -> str:
        return "hello world"

    @app.route("/state")
    def read_state() -> Union[dict, int]:
        with open(state_file, "r") as file:
            data = json.load(file)
            return jsonify(data), 200

    return app
