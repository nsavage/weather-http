import json
import serial
import requests

from datetime import datetime

host = "http://10.0.4.70:8123/api/states/sensor."
port = 8123
token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJiN2NkZDIwOTY1NjM0MTAyOTdmNTgxZTRiNDM5MTUxMSIsImlhdCI6MTYxNjI4MzcyNCwiZXhwIjoxOTMxNjQzNzI0fQ.PiQkyCk4BvzzkPp1G_XQzt7gB8ZdllcenfnKwKXJr2o"

ser = serial.Serial("/dev/ttyUSB0", 9600)
while True:
    data = ser.readline().strip()
    print(data)
    try:
        data = data.decode()
    except Exception:
        continue

    if data.startswith('$,'):
        data_list = data.split(',')

        headers = {
            "Authorization": "Bearer " + token,
            "content-type": "application/json",
        }
        data = json.dumps(
            {
                "state": data_list[1],
                "attributes": {
                    "unit_of_measurement": "°C",
                    "device_class": "temperature",
                    "friendly_name": "Shed Temperature",
                },
            }
        )
        sensor_name = "shed_temperature"
        
        try:
            response = requests.post(host + sensor_name, headers=headers, data=data)
        except Exception:
            continue
        print(response.text)

        data = json.dumps(
            {
                "state": data_list[2],
                "attributes": {
                    "unit_of_measurement": "%",
                    "device_class": "humidity",
                    "friendly_name": "Shed Humidity"
                },
            }
        )
        sensor_name = "shed_humidity"
        
        try:
            response = requests.post(host + sensor_name, headers=headers, data=data)
        except Exception:
            continue
        print(response.text)

        # now = datetime.now()
        # results = {
        #     "temperature": data_list[1],
        #     "humidity": data_list[2],
        #     "datetime": now.strftime("%d/%m/%Y %H:%M:%S"),
        # }
        # print(results)
        # with open("/tmp/temp_data.txt", "w") as outfile:
        #     json.dump(results, outfile)
   
